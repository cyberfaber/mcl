%% computes the x,y position of the endpoint of the measurements
%% input:
%%	pose == the robot pose  (x, y, theta)
%%	measurements == couples [bearing; range] (size 2xM)
%%
%%
%% output:
%%	endpoints == the [x; y] coordinates of every computed point (size 2xM)
%% 

function endpoints = project_measurements(pose, measurements)
  
  x = pose(1);
  y = pose(2);
  theta = pose(3);
  
  endpoints = zeros(2,size(measurements,2));
  endpoints(1,:) = x + measurements(2,:) .* cos(theta + measurements(1,:));
  endpoints(2,:) = y + measurements(2,:) .* sin(theta + measurements(1,:));
  
  
end