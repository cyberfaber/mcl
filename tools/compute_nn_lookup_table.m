%% Creates a lookup table where every entry has three dimensions,
%% with the y and x values of the closest occupied point in the map stored in the first two dimensions,
%% and the distance value stored in the 3rd dimension 

function lu_table = compute_nn_lookup_table(map, sigma, z_hit, z_rand, z_max)
  
  lu_table = zeros(size(map,1), size(map,2), 4);
  
  
  % check bigger and bigger squares until an occupied point is found
  for r=1:size(map,1)
    for c=1:size(map,2)
      
      disp(['<' num2str(r) ', ' num2str(c) '>']);
      fflush(stdout);
      
      found = false;  % will be set to true when the closest point in the map has been found
      
      if(map(r,c))
	lu_table(r,c,1:3) = [r c 0];
	found = true;
      end
      
     
      
      dist=sqrt(size(map,1)^2 + size(map,2)^2); % will track the minimum distance
      side = 1;
      while (found == false)
	
	% check the left side of the square
	cc = c-side;
	if (cc > 0 && cc <= size(map,2))
	  
	  for rr=r-side:r+side
	    if (rr > 0 && rr <= size(map,1))
	
	      if(map(rr,cc))
		found = true;
		this_dist = sqrt((cc-c)^2 + (rr-r)^2);
		
		if(this_dist < dist)
		  dist = this_dist;
		  lu_table(r,c,1:3) = [rr,cc, dist];
	        end
	    
	      end
	    end
	  end
	end
	
	% check the top side of the square
	rr = r-side;
	if (rr > 0 && rr <= size(map,1))
	  
	  for cc=c-side+1:c+side
	    if (cc > 0 && cc <= size(map,2))
	
	      if(map(rr,cc))
		found = true;
		this_dist = sqrt((cc-c)^2 + (rr-r)^2);
		
		if(this_dist < dist)
		  dist = this_dist;
		  lu_table(r,c,1:3) = [rr,cc,dist];
	        end
		
	      end
	    end
	  end
	end
	
	% check the right side of the square
	cc = c+side;
	if (cc > 0 && cc <= size(map,2))
	  
	  for rr=r-side+1:r+side
	    if (rr > 0 && rr <= size(map,1))
	
	      if(map(rr,cc))
		found = true;
		this_dist = sqrt((cc-c)^2 + (rr-r)^2);
		
		if(this_dist < dist)
		  dist = this_dist;
		  lu_table(r,c,1:3) = [rr,cc,dist];
	        end
	    
	      end
	    end
	  end
	end
	
	% check the bottom side of the square
	rr = r+side;
	if (rr > 0 && rr <= size(map,1))
	  
	  for cc=c-side+1:c+side-1
	    if (cc > 0 && cc <= size(map,2))
	
	      if(map(rr,cc))
		found = true;
		this_dist = sqrt((cc-c)^2 + (rr-r)^2);
		
		if(this_dist < dist)
		  dist = this_dist;
		  lu_table(r,c,1:3) = [rr,cc,dist];
	        end
		
	      end
	    end
	  end
	end
	
	side = side + 1;
	
      end % end while
          
    end % end for
        
  end % end for
  

  % pass over all the lookup table to compute the probabilities
  max_prob=0; % will track the maximum probabilty (will be used for normalization)
  for r=1:size(map,1)
    
    for c=1:size(map,2)
    
      lu_table(r,c,4) = z_hit * gauss_prob(lu_table(r,c,3), sigma) + z_rand/z_max;
      max_prob = max(max_prob, lu_table(r,c,4));
      
    end
    
  end
  
  % normalize
  lu_table(:,:,4) = lu_table(:,:,4)/max_prob;
  
end % end function