function measurements = laser2measurements(laser)

  measurements = zeros(2, numel(laser(1).ranges), numel(laser));
  
  angles = normalize_angle(deg2rad(laser(1).angular_resolution * [0:1:360]) + laser(1).start_angle);
  
  
  for i=1:numel(laser)
    
    
    measurements(:,:,i) = [angles;laser(i).ranges];
    
    
  end

end