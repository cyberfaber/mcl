function flipped = flipmap(map)
  
  flipped = zeros(size(map,1), size(map,2));
  
  flipped(:,:) = map(end:-1:1, :);
  
end