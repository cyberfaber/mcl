m = zeros(10,10);

m(2,:) = 1;

m(:, 3) = 1;

m(:, 9) = 1;


SIGMA = 2;
Z_HIT = 5;
Z_RAND = 3;
Z_MAX = 30;


l = compute_nn_lookup_table(m, SIGMA, Z_HIT, Z_RAND, Z_MAX);

disp('m:');
disp(m);
disp('');
disp('l_rows:');
disp(l(:,:,1));
disp('');
disp('l_cols:');
disp(l(:,:,2));
disp('');
disp('l_dists:');
disp(l(:,:,3));
disp('');
disp('l_probs:');
disp(l(:,:,4));
