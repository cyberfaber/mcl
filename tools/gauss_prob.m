%% probability of x under a Gaussian distribution with zero mean and standard deviation given by sigma

function prob = gauss_prob(x, sigma)
  
  prob = (1/(sigma * sqrt(2*pi))) * e^(- ((x)^2)/2*sigma^2 );

end