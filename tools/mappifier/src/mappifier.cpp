#include <iostream>
#include <fstream>
#include <string>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

std::string _output_dist_name;
std::string _output_prob_name;
float _SIGMA;
float _Z_HIT;
float _Z_RAND;
float _Z_MAX;


// computes the probability value associated to the x value, assuming a gauusian distribution centered in zero with the given sigma
template <typename T1, typename T2>
T1 gaussProb(T1 x, T2 sigma){
  
  T1 prob = (1/(sigma * sqrt(2*M_PI))) * exp(- ((x*x)/(2*(sigma * sigma)) ));

  return prob;
  
}


void init(int argc, char ** argv){
  
  if(argc < 2){
    std::cout << "Usage:" << std::endl << "mappify <image> <options>" << std::endl << std::endl;
    exit(0);
  }
  
  _output_dist_name = std::string("dists.txt");
  _output_prob_name = std::string("probs.txt");
  
  _SIGMA = 1;
  _Z_HIT = 5;
  _Z_RAND = 3;
  _Z_MAX = 30;
  
  for(unsigned int i=2; i<argc; i++){
    std::string option(argv[i]);
    
    bool known = false;
    
    if(option.compare("-dist") == 0){
      known = true;
      i++;
      if(i == argc){
	std::cerr << "ERROR: no value specified for option " << option << std::endl;
	exit(1);
      }
      _output_dist_name = std::string(argv[i]);
    }
    
    if(option.compare("-prob") == 0){
      known = true;
      i++;
      if(i == argc){
	std::cerr << "ERROR: no value specified for option " << option << std::endl;
	exit(1);
      }
      _output_prob_name = std::string(argv[i]);
    }
    
    if(option.compare("-zhit") == 0){
      known = true;
      i++;
      if(i == argc){
	std::cerr << "ERROR: no value specified for option " << option << std::endl;
	exit(1);
      }
      _Z_HIT = atof(argv[i]);
    }
    
    if(option.compare("-zrand") == 0){
      known = true;
      i++;
      if(i == argc){
	std::cerr << "ERROR: no value specified for option " << option << std::endl;
	exit(1);
      }
      _Z_RAND = atof(argv[i]);
    }
    
    if(option.compare("-zmax") == 0){
      known = true;
      i++;
      if(i == argc){
	std::cerr << "ERROR: no value specified for option " << option << std::endl;
	exit(1);
      }
      _Z_MAX = atof(argv[i]);
    }
    
    if(option.compare("-sigma") == 0){
      known = true;
      i++;
      if(i == argc){
	std::cerr << "ERROR: no value specified for option " << option << std::endl;
	exit(1);
      }
      _SIGMA = atof(argv[i]);
    }
    
    if(!known){
      std::cerr << "ERROR: unknown command: " << option << std::endl;
      exit(1);
    }
  }
}


int main(int argc, char ** argv){
  
  init(argc, argv);
  
  cv::Mat image = cv::imread(argv[1], CV_LOAD_IMAGE_GRAYSCALE);
  
  if(!image.data){
    std::cout << "ERROR opening the image" << std::endl;
    exit(1);
  }
  
  std::cout << "image is " << image.rows << "x" << image.cols << std::endl;
  
  // prepare an empty map
  cv::Mat_<bool> map(image.rows, image.cols, false);
  
  // fill the map
  for(unsigned int r=0; r<image.rows; r++){
    for(unsigned int c=0; c<image.rows; c++){
      
      if((int)(image.at<uchar>(r,c)) < 200){
	map.at<bool>(r,c) = 1;
      }
      
    }
  }
  
  int rows = image.rows;
  int cols = image.cols;
  int total_points = rows*cols;
  
  float * distances = (float *) malloc(sizeof(float) * total_points);
  float * probabilities = (float *) malloc(sizeof(float) * total_points);
  
  float max_dist = sqrt(pow(cols,2) + pow(rows,2));
  float greater_dist = 0;
  
  for (int r=0; r<rows; r++){
    
    unsigned int offset = r * cols;
    if(r % 100 == 0){
      std::cout << "at row " << r << std::endl;
    }
    
    for(int c=0; c<cols; c++){
      
      // std::cout << "map(" << r << ", " << c << ") : " << map.at<unsigned int>(r,c) << std::endl;
      
      unsigned int index = offset + c;
      
      //      std::cout << r << ", " << c << std::endl;
      
      bool found = false;  // will be set to true when the closest point in the map has been found
      
            
      if(map.at<bool>(r,c)){
	distances[index] = 0;
	found = true;
      }
            
      
      float dist = max_dist; // will track the minimum distance
      
      int side = 1;
      int cc, rr;
      while(!found || side < dist){
	
	// check the left side of the square
	cc = c-side;
	if (cc >= 0 && cc < cols){
	  
	  for (rr=r-side; rr<=r+side; rr++){
	    if (rr > 0 && rr <= rows){
	      
	      if(map.at<bool>(rr,cc)){
		found = true;
		float this_dist = sqrt((cc-c)*(cc-c) + (rr-r)*(rr-r));
		
		if(this_dist < dist){
		  dist = this_dist;
		  distances[index] = dist;
		  
		  //		  std::cout << "dist:" << dist << "\tdistances[index]: " << distances[index] << std::endl;
		}
		
	      }
	      
	    }
	  }
	}
	
	// check the top side of the square
	rr = r-side;
	if (rr >= 0 && rr < rows){
	  
	  for (cc=c-side; cc<=c+side; cc++){
	    if (cc > 0 && cc <= cols){
	      
	      if(map.at<bool>(rr,cc)){
		found = true;
		float this_dist = sqrt((cc-c)*(cc-c) + (rr-r)*(rr-r));
		
		if(this_dist < dist){
		  dist = this_dist;
		  distances[index] = dist;
		}
		
	      }
	    }
	  }
	}
	
	// check the right side of the square
	cc = c+side;
	if (cc >= 0 && cc < cols){
	  
	  for (rr=r-side; rr<=r+side; rr++){
	    if (rr > 0 && rr <= rows){
	      
	      if(map.at<bool>(rr,cc)){
		found = true;
		float this_dist = sqrt((cc-c)*(cc-c) + (rr-r)*(rr-r));
		
		if(this_dist < dist){
		  dist = this_dist;
		  distances[index] = dist;
		}
		
	      }
	    }
	  }
	}
	
	// check the bottom side of the square
	rr = r+side;
	if (rr >= 0 && rr < rows){
	  
	  for (cc=c-side; cc<=c+side; cc++){
	    if (cc > 0 && cc <= cols){
	      
	      if(map.at<bool>(rr,cc)){
		found = true;
		float this_dist = sqrt((cc-c)*(cc-c) + (rr-r)*(rr-r));
		
		if(this_dist < dist){
		  dist = this_dist;
		  
		  distances[index] = dist;
		}
		
	      }
	    }
	  }
	}
	
	side = side + 1;
	
	if(found && greater_dist < dist){
	  greater_dist = dist;
	}
	
	
      }
      
      
    }
    
  }
  
  std::cout << "distances computed" << std::endl;
  
  // now compute the probabilities on the basis of the distances
  float max_prob = 0;
  for(unsigned int i=0; i<total_points; i++){
    // std::cout << "distances[" << i << "]: " << distances[i];
    probabilities[i] = _Z_HIT * gaussProb(distances[i], _SIGMA) + + _Z_RAND/_Z_MAX;
    if(max_prob < probabilities[i]){
      max_prob = probabilities[i];
    }
    
    // std::cout << "distance: " << distances[i] << "\tprobability: " << probabilities[i] << std::endl;
  }
  
  // normalize all the probabilities
  for(unsigned int i=0; i<total_points; i++){
    probabilities[i] = probabilities[i]/max_prob;
  }
  
  
  std::cout << "prepearing to show" << std::endl;
  
  // just for showing on screen
  cv::Mat im_dist(rows, cols, CV_8UC1, cv::Scalar(0));
  cv::Mat im_prob(rows, cols, CV_8UC1, cv::Scalar(0));
  
  for(int r = 0; r<rows; r++){
    int offset = r*cols;
    for(int c = 0; c<cols; c++){
      int index = offset+c;
      
      im_dist.at<uchar>(r,c) = (float)((float)distances[index]/greater_dist) * 255;
      im_prob.at<uchar>(r,c) = (float)(probabilities[index]) * 255 ;
      
      // std::cout << distances[index] << std::endl;
      // std::cout << probabilities[index] << std::endl;
      
    }
  }
  
  
  cv::namedWindow("image", cv::WINDOW_AUTOSIZE);
  cv::namedWindow("map", cv::WINDOW_AUTOSIZE);
  cv::namedWindow("distances", cv::WINDOW_AUTOSIZE);
  cv::namedWindow("probabilities", cv::WINDOW_AUTOSIZE);
  
  std::cout << "ready to show" << std::endl;
  
  imshow("image", image);
  imshow("map", map);
  imshow("distances", im_dist);
  imshow("probabilities", im_prob);
  
  cv::waitKey(0);
  
  std::cout << "writing on files" << std::endl;
  std::ofstream distout(_output_dist_name.c_str());
  std::ofstream probout(_output_prob_name.c_str());
  
  for(unsigned int r=0; r<rows; r++){
    unsigned int offset = r*cols;
    
    for(unsigned int c = 0; c<cols; c++){
      unsigned int index = offset + c;
      
      distout << " " << distances[index];
      probout << " " << probabilities[index];
      
    }
    distout << std::endl;
    probout << std::endl;
  }
  
  return 0;
}
