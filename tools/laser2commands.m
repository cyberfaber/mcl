function commands = laser2commands(laser)

steps = numel(laser);

commands = zeros(3,steps);

commands(:,1) = [0 0 0];

for i=2:steps
  
  prevpose = laser(i-1).pose;
  newpose = laser(i).pose;
    
  prevM = v2t(prevpose);
  newM = v2t(newpose);

  relative = t2v(inv(prevM) * newM);
  
  r1 = atan2(relative(2),relative(1));
  t = sqrt(relative(2)^2 + relative(1)^2);
  r2 = normalize_angle(relative(3) - r1);
  
  commands(:,i) = [r1, t, r2];
  
end
