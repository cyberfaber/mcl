function rad = deg2rad(degrees)

  rad = degrees*pi/180;

end