%% reads a map from an imagefile.
%% black points are considered occupied

function map = read_map(imagename)

  im = imread(imagename);
  
  map = zeros(size(im,1), size(im,2));
  
  for r=1:size(im,1)
    
    for c=1:size(im,2)
      
      if(im(r,c) < 200)
	map(r,c) = 1;
      end
      
    end

  end

end