function weights = compute_weights(particles, measurements, probmap, max_range)
  
  numParticles = numel(particles);
  weights = zeros(numParticles, 1);
  
  M = size(measurements,2);
  
  for i=1:numParticles
    
    endpoints = floor(project_measurements(particles(i).pose, measurements));
    
    weights(i) = 1;
    
    for p=1:M
      if (measurements(2,p) > max_range)
	continue;
      end
      if(endpoints(1,p) < 1 || endpoints(1,p) > size(probmap, 2) || endpoints(2,p) < 1 || endpoints(2,p) > size(probmap, 1))
	continue;
      end
      
      weights(i) = weights(i) * probmap(endpoints(2,p), endpoints(1,p));
    end
    
  end

end