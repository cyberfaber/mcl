function plot_particles(particles, max_y)
  
  numParticles = numel(particles);
  
  triang_size = 4;
  triangle = [-triang_size,-triang_size,1;
	      -triang_size,triang_size,1;
	      triang_size+triang_size,0,1]';
  
  for i=1:numParticles
    
    M = v2t(particles(i).pose);
    
    v = M*triangle;
    
    v = [v v(:,1)];
    
    plot(v(1,:), max_y-v(2,:),'LineWidth', 2);
    hold on;
  end
  
end