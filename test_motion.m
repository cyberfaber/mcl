addpath('tools')
load('laser_data');

laser2commands;

noise = [0.005, 0.01, 0.005];

particles = struct;

particles(1).pose = [0, 0, 0];
particles(2).pose = [0 0 pi];

for i=1:numel(commands)

  particles = motion_rtr(particles, commands(:,i), noise);
  hold off;
  plot_particles(particles);
  sleep(0.01);

end