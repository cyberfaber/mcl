clear all;
close all;
hold off;

addpath('tools');

disp('loading dataset')
load('data/csail_laser_data');
commands = laser2commands(laser);
measurements = laser2measurements(laser);
% distmap = load('data/csail_dists.txt');
probmap = load('data/csail_probs.txt');
probmap = flipmap(probmap);
mapimage = imread('data/csail.corrected.png');

max_y = size(mapimage, 1); % used for visualization purposes 

NOISE = [0.005 0.01 0.005]; % noise in the [rot1 trasl rot2] commands
MAX_RANGE = laser(1).maximum_range;

particles = struct;

numParticles = 100;

for i=1:numParticles
  
  x = rand * size(mapimage,2);
  y = rand * size(mapimage,1);
  theta = rand * 2 * pi;
  
  particles(i).pose = [x, y, theta];
  
end

for i=100:numel(commands)

  particles = motion_rtr(particles, commands(:,i), NOISE);
  
  hold off;
  
  imshow(mapimage, [min(min(mapimage)) max(max(mapimage))]);
  
  hold on;
  
  plot_particles(particles, max_y);
  
  ginput(1);
  
  weights = compute_weights(particles, measurements(:,:,i), probmap, MAX_RANGE);
  
  particles = resample(particles, weights)
  
  hold off;
  
  imshow(mapimage, [min(min(mapimage)) max(max(mapimage))]);
  
  hold on;
  
  plot_particles(particles, max_y);
  
  
  ginput(1);
end